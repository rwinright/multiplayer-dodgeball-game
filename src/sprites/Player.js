import { Physics } from 'phaser';

import Helpers from '../helpers/helpers';

export default class extends Physics.Arcade.Sprite {
	constructor(scene, x, y, texture) {
		super(scene, x, y, texture);

		scene.add.existing(this);
		scene.physics.add.existing(this);
		scene.physics.world.enable(this);
		//For debugging
		this.setCollideWorldBounds(true);

		this.direction = 0;
		this.facing = "north"
		this.movement = new Phaser.Math.Vector2(0, 0);
		this.hasBall = false;
		this.cursor = this.scene.input.activePointer; //Grab the pointer on the screen.

	}

	create() {
		const playerAnims = [
			{
				frameName: "Character_Up",
				start: 0,
				end: 3,
				frameRate: 15,
				repeat: -1
			},
			{
				frameName: "Character_UpRight",
				start: 0,
				end: 3,
				frameRate: 15,
				repeat: -1
			},
			{
				frameName: "Character_Right",
				start: 0,
				end: 3,
				frameRate: 15,
				repeat: -1
			},
			{
				frameName: "Character_DownRight",
				start: 0,
				end: 3,
				frameRate: 15,
				repeat: -1
			},
			{
				frameName: "Character_Down",
				start: 0,
				end: 3,
				frameRate: 15,
				repeat: -1
			},
			{
				frameName: "Character_DownLeft",
				start: 0,
				end: 3,
				frameRate: 15,
				repeat: -1
			},
			{
				frameName: "Character_Left",
				start: 0,
				end: 3,
				frameRate: 15,
				repeat: -1
			},
			{
				frameName: "Character_UpLeft",
				start: 0,
				end: 3,
				frameRate: 15,
				repeat: -1
			},

		];

		Helpers().generateAnimations(playerAnims, this.scene, this.texture.key);
		this.setScale(2).setSize(10, 8).setOffset(10, 15);
		this.setBounce(1);

	}

	update(keys) {

		// Create vector 2 movement, and normalize the angular movement.
		//TODO: Need to also create a setting for game pads.
		this.movement.set(
			keys.D.isDown - keys.A.isDown, //Set x
			keys.S.isDown - keys.W.isDown  //Set y
		).normalize();
		//Set the player's movement.
		this.setVelocity(
			this.movement.x * 200,
			this.movement.y * 200
		)

		//Set facing and mouse aiming
		let angle = Phaser.Math.Angle.Between(this.x, this.y, this.cursor.worldX, this.cursor.worldY);
		//8 and 0 have to be the same. Don't ask questions, though there's a simple answer
		const dirs = { 0: "Character_Left", 1: "Character_UpLeft", 2: 'Character_Up', 3: 'Character_UpRight', 4: 'Character_Right', 5: "Character_DownRight", 6: "Character_Down", 7: "Character_DownLeft", 8: "Character_Left" }
		this.facing = dirs[Phaser.Math.RoundTo((Phaser.Math.RadToDeg(angle) + 180) / 45, 0)]

		//Play animations
		this.anims.play(this.facing, (this.movement.x || this.movement.y));
		//Throwing balls and stuff
		if (this.cursor.leftButtonDown()) {
			this.throwBall(angle);
		}
	}

	//Property that throws balls and add colliders to balls.
  throwBall(angle) {
		let ball = this.scene.ballGroup.create(this.x, this.y, this.texture.key).setScale(1);
		this.scene.physics.moveTo(ball, this.cursor.x, this.cursor.y, 400)
    ball.rotation = angle;
	}
}