import { Scene, Cameras } from "phaser";
import { CST } from "../../CST";
import Player from "../../sprites/Player";

export default class Level1 extends Scene{
  constructor(){
    super({key: CST.SCENES.GAME.LEVEL1})
    this.collisionLayers = [];
  }

  preload(){

  }

  //BIG TODO: Make a helper function for those cursed collisions...
  create(){
    console.log("Level 1 Open");
    //Set the map
    const map = this.make.tilemap({ key: 'woodland-atlas' });
    const tiles = map.addTilesetImage('woodland', 'woodland-tiles', 32, 32);
    const layerNames = ['Ground', 'GroundDecoration', 'Platform', 'Platform2', 'Walls', 'Dividers'];
    

    //This needs some hardcore refactoring
    layerNames.forEach(layerName => {
      if(layerName === "Walls" || layerName === "Dividers"){ //Layernames that need collisions
        //Temporary variable for the collision layer;
        let colLayer = map.createStaticLayer(layerName, tiles, 0, 0).setCollisionByExclusion([-1]);
        //Put the layers requiring collision into the outer array so the player can access it.aw
        this.collisionLayers.push(colLayer);
      } else if(layerName === "Platform2"){ //If the piece of the wall is on this layer, set the depth over the character so he goes "behind" it.
        map.createStaticLayer(layerName, tiles, 0, 0).setDepth(1)
      } else {
        map.createStaticLayer(layerName, tiles, 0, 0);
      }
    });

    //Create player
    this.player = new Player(this, 400, 400, 'player');
    this.player.create();
    this.keys = this.input.keyboard.addKeys("W, S, A, D, P, E, Q, SPACE");
    //Add collisions to character and level
    this.physics.add.collider(this.player, this.collisionLayers);

    //For the balls
    this.ballGroup = this.physics.add.group({ runChildUpdate: true });
    this.physics.add.collider(this.ballGroup, this.collisionLayers[0])
    this.physics.add.collider(this.ballGroup, this.ballGroup)
  }

  update(){
    this.player.update(this.keys);
  }
}